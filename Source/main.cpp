#include "../Include/LoopExecutor.hpp"
#include <thread>

int* longForLoopBubbleA(int* data, ulong loopRange)
{
    ulong i = 0;
    ulong j = 0;
    int temporary = 0;

    for (; i < loopRange; i++)
    {
        for (j = 0; j < loopRange - i; j++)
        {
            if(data[j] > data[j+1])
            {
                temporary = data[j];
                data[j] = data[j+1];
                data[j+1] = temporary;
            }
        }
    }

    return data;
}

int* longForLoopBubbleB(int* data, ulong loopRange)
{
    ulong i = 0;
    ulong j = 0;
    int temporary = 0;

    for (; i < loopRange; i++)
    {
        for (j = 0, temporary = 0; j < loopRange - i; j++)
        {
            if(data[j] > data[j+1])
            {
                temporary = data[j];
            }

            if(data[j] > data[j+1])
            {
                data[j] = data[j+1];
            }

            if(temporary > data[j+1])
            {
                data[j+1] = temporary;
            }
        }
    }

    return data;
}

int* longForLoopBubbleC(int* data, ulong loopRange)
{
    ulong i = 0;
    ulong j = 0;
    int temporary = 0;

    for (; i < loopRange; i++)
    {
        for (j = 0, temporary = 0; j < loopRange - i; j++)
        {
            temporary = data[j] > data[j+1] ? data[j] : temporary;
            data[j] = data[j] > data[j+1] ? data[j+1] : data[j];
            data[j+1] = temporary > data[j+1] ?  temporary : data[j+1];
        }
    }

    return data;
}

int* longForLoopBubbleD(int* data, ulong loopRange)
{
    ulong i = 0;
    ulong j = 0;

    int temporary_A = 0;
    int temporary_B = 0;

    for (; i < loopRange; i++)
    {
        for (j = 0; j < loopRange - i; j++)
        {
            temporary_A = data[j];
            temporary_B = data[j+1];

            data[j] = temporary_B < temporary_A ? temporary_B : temporary_A;
            data[j+1] = temporary_B < temporary_A ? temporary_A : temporary_B;
        }
    }

    return data;
}

int main()
{
    uint32_t number_of_loop_executions = 1;
    std::vector<uint64_t> data_size
    {
        100000
    };

    auto **loops = new LoopExecutor *[data_size.size()];
    loops[0] = new LoopExecutor(longForLoopBubbleD, number_of_loop_executions, data_size[0]-1, data_size[0]);

    for (size_t i = 0; i != data_size.size(); ++i)
    {
        (*loops[i])();
    }

    LoopExecutor::generateOutputFile(std::string("./"), loops, data_size.size());

    return 0;
}
