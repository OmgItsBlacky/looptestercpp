#include "../Include/CSVFileCreator.hpp"

#include <chrono>
#include <regex>

CSVFile::CSVFileCreator::CSVFileCreator(const std::string& path)
{
    std::string outfile_path = path+default_file_name_+getTimeStamp();
    output_file_.open(outfile_path, std::ios::app);
}

void CSVFile::CSVFileCreator::appendLineToFile(std::string data)
{
    output_file_ << data << std::endl;
}

void CSVFile::CSVFileCreator::appendDataToFile(std::string data)
{
    output_file_ << data;
}

std::string CSVFile::CSVFileCreator::getTimeStamp() const
{
    auto time_point = std::chrono::system_clock::now();
    std::time_t time = std::chrono::system_clock::to_time_t(time_point);
    std::string time_stamp = std::ctime(&time);

    const auto target = std::regex{" "};
    const auto replacement = std::string{"_"};

    time_stamp.erase(time_stamp.size()-1, time_stamp.size());
    time_stamp += ".csv";
    time_stamp = regex_replace(time_stamp, target, replacement);
    time_stamp.erase(0, 4);
    return time_stamp;
}

CSVFile::CSVFileCreator::~CSVFileCreator()
{
    output_file_.close();
}
