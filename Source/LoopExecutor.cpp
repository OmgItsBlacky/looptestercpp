#include "../Include/LoopExecutor.hpp"

#include <numeric>

LoopExecutor::LoopExecutor(std::function<int*(int* data, ulong loop_range)> executed_loop,
        uint64_t number_of_loop_executions,
        uint64_t loop_range,
        uint64_t data_size)
        :
        loop_range_(loop_range),
        number_of_loop_executions_(number_of_loop_executions),
        data_size_(data_size),
        executed_loop_(std::move(executed_loop))
{
    data_ = new int[data_size_];

    std::ifstream data;
    data.open("Random.txt");
    std::string number;

    for(uint64_t i = 0; i < data_size; i++)
    {
        std::getline(data,number);
        data_[i] = std::stoi(number);
    }

    data.close();
}

void LoopExecutor::operator()()
{
    uint64_t iter = 0;

//
//    for(uint64_t i = 0; i < data_size_; i++)
//    {
//        std::cout << data_[i] << std::endl;
//    }

    for (; iter!=number_of_loop_executions_; ++iter) {
        auto start = std::chrono::high_resolution_clock::now();
        data_ = executed_loop_(data_, loop_range_);
        auto stop = std::chrono::high_resolution_clock::now();
        loop_execution_duration_.push_back(stop-start);
    }
//
//    for(uint64_t i = 0; i < data_size_; i++)
//    {
//        std::cout << data_[i] << std::endl;
//    }

    displayInformation();
}

LoopExecutor::~LoopExecutor()
{
    delete data_;
}

uint64_t LoopExecutor::getLoopRange() const
{
    return loop_range_;
}

uint64_t LoopExecutor::getDataSize() const
{
    return data_size_;
}

std::vector<std::chrono::nanoseconds> LoopExecutor::getLoopExecutionDuration() const
{
    return loop_execution_duration_;
}

double LoopExecutor::getAverageTimeInNanoseconds() const
{
    auto timings = std::vector<int64_t>();
    for (const auto& loop_time: getLoopExecutionDuration()) {
        timings.push_back(std::chrono::duration_cast<std::chrono::nanoseconds>(loop_time).count());
    }

    return std::accumulate(timings.begin(), timings.end(), 0.0)/number_of_loop_executions_;
}

double LoopExecutor::getAverageTimeInMilliseconds() const
{
    return getAverageTimeInNanoseconds()/1e10;
}

void LoopExecutor::displayInformation() const
{
    std::cout << "Loop executions:" << std::setw(6) << getNumberOfLoopExecutions()
              << "\tLoop range:" << std::setw(12) << getLoopRange()
              << "\tData size:" << std::setw(12) << getDataSize()
              << "\tAverage time of single loop execution: " << std::setw(12) << getAverageTimeInNanoseconds()
              << " [ns]"
              << std::endl << std::endl;
}

uint64_t LoopExecutor::getNumberOfLoopExecutions() const
{
    return number_of_loop_executions_;
}

std::string LoopExecutor::getCSVInformation() const
{
    std::string result =
            std::to_string(getNumberOfLoopExecutions())+","+
                    std::to_string(getLoopRange())+","+
                    std::to_string(getDataSize())+","+
                    std::to_string(getAverageTimeInNanoseconds())+","+
                    std::to_string(getAverageTimeInNanoseconds()/getLoopRange())+"\n";

    return result;
}

void LoopExecutor::generateOutputFile(const std::string& path, LoopExecutor** loop_executors, uint32_t number_of_tests)
{
    CSVFile::CSVFileCreator outfile_creator(path);
    std::string header =
            "Loop executions,"
            "Loop range,"
            "Data size,"
            "Average time of single loop execution,"
            "Average time of single iteration of loop";

    outfile_creator.appendLineToFile(header);

    std::string data;
    for (uint32_t i = 0; i<number_of_tests; ++i) {
        data += loop_executors[i]->getCSVInformation();
    }
    outfile_creator.appendDataToFile(data);
}