#ifndef LOOPTESTCPP_LOOPEXECUTOR_HPP
#define LOOPTESTCPP_LOOPEXECUTOR_HPP

#include <iostream>
#include <functional>
#include <chrono>
#include <cstdint>
#include <vector>
#include <iomanip>

#include "CSVFileCreator.hpp"

class LoopExecutor {
public:
    explicit LoopExecutor() = default;

    LoopExecutor(std::function<int* (int* data, ulong loop_range)> executed_loop,
            uint64_t number_of_loop_executions, uint64_t loop_range, uint64_t data_size);

    void operator()();

    std::vector<std::chrono::nanoseconds> getLoopExecutionDuration() const;

    uint64_t getLoopRange() const;

    uint64_t getDataSize() const;

    uint64_t getNumberOfLoopExecutions() const;

    double getAverageTimeInNanoseconds() const;

    double getAverageTimeInMilliseconds() const;

    void displayInformation() const;

    std::string getCSVInformation() const;

    static void generateOutputFile(const std::string& path, LoopExecutor** loop_executors, uint32_t number_of_tests);

    ~LoopExecutor();

private:
    int* data_;
    uint64_t loop_range_;
    uint64_t number_of_loop_executions_;
    uint64_t data_size_;
    std::function<int*(int* data, ulong loop_range)> executed_loop_;
    std::vector<std::chrono::nanoseconds> loop_execution_duration_;
};

#endif //LOOPTESTCPP_LOOPEXECUTOR_HPP
