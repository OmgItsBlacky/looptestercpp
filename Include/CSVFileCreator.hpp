#ifndef LOOPTESTERCPP_CSVFILECREATOR_HPP
#define LOOPTESTERCPP_CSVFILECREATOR_HPP

#include <iostream>
#include <fstream>

namespace CSVFile {

class CSVFileCreator {
public:
    explicit CSVFileCreator(const std::string& path);

    ~CSVFileCreator();

    void appendLineToFile(std::string data);

    void appendDataToFile(std::string data);

private:
    static constexpr char const* default_file_name_ = "CPP_LOOP_TESTER_OUTPUT_";
    std::ofstream output_file_;
    std::string getTimeStamp() const;
};
}

#endif //LOOPTESTERCPP_CSVFILECREATOR_HPP
